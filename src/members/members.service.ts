import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { Member } from './entities/member.entity';

@Injectable()
export class MembersService {
  constructor(
    @InjectRepository(Member) private membersRepository: Repository<Member>,
  ) {}
  create(createMemberDto: CreateMemberDto) {
    return this.membersRepository.save(createMemberDto);
  }

  findAll() {
    return this.membersRepository.find();
  }

  findOne(id: number) {
    return this.membersRepository.findOne({ where: { id } });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    const member = await this.membersRepository.findOneBy({ id });
    if (!member) {
      throw new NotFoundException();
    }
    const updatedMember = { ...member, ...updateMemberDto };
    return this.membersRepository.save(updatedMember);
  }

  async remove(id: number) {
    const member = await this.membersRepository.findOneBy({ id });
    if (!member) {
      throw new NotFoundException();
    }
    return this.membersRepository.softRemove(member);
  }
}
