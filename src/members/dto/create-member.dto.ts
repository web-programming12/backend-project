import { IsNotEmpty, Length, Min } from 'class-validator';

export class CreateMemberDto {
  @IsNotEmpty()
  @Length(3, 64)
  name: string;

  @IsNotEmpty()
  @Min(0)
  point: number;
}
